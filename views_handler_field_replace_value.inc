<?php

class views_handler_field_replace_value extends views_handler_field {
  function query() {
    // dummy
  }

  function option_definition() {
    $options = parent::option_definition();
    $options['parent_field'] = array('default' => '');
    $options['replace_values'] = array('default' => '');
    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $all_fields = $this->view->display_handler->get_field_labels();
    $field_options = array_slice($all_fields, 0, array_search($this->options['id'], array_keys($all_fields)));

    $form['parent_field'] = array(
      '#type' => 'select',
      '#title' => t('Field'),
      '#options' => $field_options,
      '#default_value' => $this->options['parent_field'],
    );

    $form['replace_values'] = array(
      '#type' => 'textarea',
      '#title' => t('Replaced values'),
      '#description' => t('One replace value to line. Format: <code>from=>to</code>. Example:<br /><code>1=>One<br />2=>Two</code>'),
      '#default_value' => $this->options['replace_values'],
    );
  }

  function render($values) {
    static $replace_values = array();

    $field_name = $this->options['id'];
    $parent_name = $this->options['parent_field'];

    if (!isset($replace_values[$field_name])) {
      $replace_values[$field_name] = array();
      foreach (explode("\n", trim($this->options['replace_values'])) as $line) {
        list($from, $to) = explode('=>', $line);
        $replace_values[$field_name][$from] = $to;
      }
    }

    $parent_field_value = $this->view->style_plugin->rendered_fields[$this->view->row_index][$parent_name];

    if (isset($replace_values[$field_name][$parent_field_value])) {
      return $replace_values[$field_name][$parent_field_value];
    }

    return $parent_field_value;
  }
}
