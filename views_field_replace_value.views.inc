<?php

/**
 * Implements hook_views_data().
 */
function views_field_replace_value_views_data() {
  return array(
    'views' => array(
      'replace_value' => array(
         'title' => t('Re-write field value'),
         'help' => t('Re-write field value'),
         'field' => array('handler' => 'views_handler_field_replace_value'),
      ),
    ),
  );
}
